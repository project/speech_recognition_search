CONTENTS OF THIS FILE
----------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Maintainers


INTRODUCTION
------------

Multilingual Voice Search is a module made from the necessity of 
having a configurable module that would check terms for duplicates, 
including in already existing vocabularies


REQUIREMENTS
------------

No requirements


INSTALLATION
------------

    1. Copy multilingual_voice_search folder to modules directory
    2. At Administration >> Extend, enable the Multilingual Voice Search.


CONFIGURATION
-------------

The configuration page for this module is at:
  Administration >> Configuration >> Content 
  Authoring >> Multilingual Voice Search Config


MAINTAINERS
-----------

Maintainer: miguelpamferreira (https://www.drupal.org/u/miguelpamferreira)
