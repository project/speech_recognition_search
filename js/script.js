(function ($) {
  'use strict';
  Drupal.behaviors.multilingual_voice_search = {
    attach: function (context, settings) {
      if (drupalSettings.multilingual_voice_search && drupalSettings.multilingual_voice_search.enabled) {

        let click_selector = drupalSettings.multilingual_voice_search.click_selector;
        let active_class = drupalSettings.multilingual_voice_search.active_class;
        let input_selector = drupalSettings.multilingual_voice_search.input_selector;
        let redirect_url = drupalSettings.multilingual_voice_search.redirect_url;
        let url_parameter = drupalSettings.multilingual_voice_search.url_parameter;
        let default_input_text = $(input_selector).val();
        if (window.hasOwnProperty('webkitSpeechRecognition') || window.hasOwnProperty('SpeechRecognition')) {
          $(drupalSettings.multilingual_voice_search.click_selector).fadeIn();

          var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;

          var recognition = new SpeechRecognition();
          
          recognition.onstart = function () {
            $(input_selector).val('...');
            $(click_selector).addClass(active_class);
          };

          recognition.onend = function (e) {
            let input_val = $(input_selector).val();
            $(click_selector).removeClass(active_class);
            if (input_val != '...' && input_val != '') {
              window.location.href = redirect_url + '?' + url_parameter + '=' + input_val;
            } else {
              $(input_selector).val(default_input_text);
            }
          };

          recognition.onresult = function (e) {
            $(input_selector).val(e.results[0][0].transcript);
            recognition.stop();
          };

          function deactivate(ev) {
            ev.stopPropagation();
            ev.stopImmediatePropagation();
            recognition.abort();
            $(click_selector).removeClass(active_class);
            $(input_selector).val(default_input_text);
          }

          $(click_selector).click(function (ev) {
            if (!$(click_selector).hasClass(active_class)) {
              recognition.continuous = false;
              recognition.interimResults = false;
              recognition.lang = drupalSettings.multilingual_voice_search.langcode;
              recognition.start();
              ev.stopPropagation();
              ev.stopImmediatePropagation();
            } else {
              deactivate(ev);
            }
          });

          $(input_selector).click(function (ev) {
            if ($(click_selector).hasClass(active_class)) {
              deactivate(ev);
            }
          });
        }
      }
    }
  };
})(jQuery);
