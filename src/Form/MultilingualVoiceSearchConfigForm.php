<?php

namespace Drupal\multilingual_voice_search\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ImportForm.
 *
 * @package Drupal\multilingual_voice_search\Form
 */
class MultilingualVoiceSearchConfigForm extends FormBase implements FormInterface {

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Construct method for service instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('contain.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'multilingual_voice_search_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'multilingual_voice_search.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#title'] = $this->t('Multiligual Voice Search Config');

    $config = $this->config('multilingual_voice_search.settings');

    $field_name = 'enabled';
    $form[$field_name] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Multilingual Voice Search'),
      '#default_value' => ($config->get($field_name) !== NULL) ? $config->get($field_name) : FALSE,
      '#description' => $this->t('Whether to enable this module. If unchecked the module will hide the click element.'),
    ];

    $field_name = 'click_selector';$this
      ->$form[$field_name] = [
        '#type' => 'textfield',
        '#title' => $this->t('Click jQuery Selector'),
        '#default_value' => ($config->get($field_name) && !empty($config->get($field_name))) ? $config->get($field_name) : '',
        '#description' => $this->t('jQuery selector for activation of voice search on click. 
        The element should be added with display none and will be show if the module is enabled and the API is available'),
        '#required' => TRUE,
      ];

    $field_name = 'active_class';
    $form[$field_name] = [
      '#type' => 'textfield',
      '#title' => $this->t('Active Search Class'),
      '#default_value' => ($config->get($field_name) && !empty($config->get($field_name))) ? $config->get($field_name) : '',
      '#description' => $this->t('Class for indicating that voice search is active. This class will be applied to the element corresponding to Click jQuery Selector.'),
      '#required' => TRUE,
    ];

    $field_name = 'input_selector';
    $form[$field_name] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text Input Selector'),
      '#default_value' => ($config->get($field_name) && !empty($config->get($field_name))) ? $config->get($field_name) : '',
      '#description' => $this->t('jQuery Selector for input where spoken text should be inserted'),
      '#required' => TRUE,
    ];

    $field_name = 'redirect_url';
    $form[$field_name] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect URL'),
      '#default_value' => ($config->get($field_name) && !empty($config->get($field_name))) ? $config->get($field_name) : '',
      '#description' => $this->t('Field for indicating redirect URL. If empty, this field will be ignored and a form exists around the input, the form will be submitted instead.'),
    ];

    $field_name = 'url_parameter';
    $form[$field_name] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL Parameter'),
      '#default_value' => ($config->get($field_name) && !empty($config->get($field_name))) ? $config->get($field_name) : '',
      '#description' => $this->t('Name of the GET parameter to be used on redirect. If empty, this field will be ignored and a form exists around the input, the form will be submitted instead.'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    if ($values['form_id'] == 'multilingual_voice_search_config') {
      $config = $this->configFactory->getEditable('multilingual_voice_search.settings');

      $values_ignored = [
        'submit',
        'form_build_id',
        'form_token',
        'form_id',
        'op',
      ];
      foreach ($values as $name => $value) {
        if (!in_array($name, $values_ignored)) {
          $config->set($name, $value);
        }
      }
      $config->save();
    }

    $form_state->setRebuild();
  }

}
